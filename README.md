# Auteur : Michaël Simoneau
# Date : 07-06-2000
# Version: 1.0.0

## Compilation et execution

### Eiffel Studio pour Windows
Téléchargement: https://www.eiffel.org/downloads

### Eiffel Studio pour Linux
Entrez ces commandes dans une console:
- sudo add-apt-repository ppa:eiffelstudio-team/ppa
- sudo apt-get update
- sudo apt-get install eiffelstudio

### Une fois Eiffel Studio intallé
À partir d'Eiffel Studio : 
- Ouvrir « tetris_server.ecf » dans Eiffel à partir du dossier « Tetris_Server »
- Ouvrir « Tetris_Attack.ecf » dans Eiffel à partir du dossier « Tetris_Attack »
- Glisser tous les fichiers du dossier « DLL64 » dans le dossier « Tetris_Attack »
- Compiler les deux projets Eiffel à l’aide du bouton « Compile » dans la barre d’adresse
- Lancer le serveur, puis le jeu à l’aide du bouton vert « Play » dans la barre d’adresse


Après la première compilation, il sera possible de lancer le jeu sans Eiffel en 
allant dans « tetris-attack-remastered\Tetris_Attack\EIFGENs\tetris_attack\W_code », 
en y glissant les fichiers DDL64 et en ouvrant le fichier exécutable.

## Comment jouer

### Fonctionnement
Les spécifications du jeu sont :
- Il faut aligner au moins trois blocs de même couleur verticalement ou horizontalement pour les éliminer;
- Les blocs sont affectés par la gravité et tomberont si aucun bloc ne les supporte;
- Des rangées de blocs peuvent être générées à la demande du joueur;
- Un bloc éliminé donne 10 points, alors qu’une rangée générée donne 1 point.


### Contrôles
Les contrôles du jeu sont :
- Les flèches du clavier contrôlent le curseur, une case à la fois;
- La barre d’espace fait interchanger les blocs situés dans le curseur;
- Le bouton « CTRL » de droite permet de générer une rangée de blocs au bas du tableau.

### But
Le but est d'éliminer le plus de blocs possible dans la limite de temps pour
marquer le plus grand nombre de points.
