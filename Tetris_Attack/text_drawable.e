﻿note
	description: "Une classe de création de textures de textes."
	author: "Michaël Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"

class
	TEXT_DRAWABLE


feature -- Implémentation

	create_text_texture(a_renderer:GAME_RENDERER; a_font:TEXT_FONT; a_text:STRING; a_color:GAME_COLOR):GAME_TEXTURE
			-- Créer une {GAME_TEXTURE} en prennant en compte de "a_text", "a_font" et "a_color"
		require
			valid_renderer: not a_renderer.has_error
			valid_font: not a_font.has_error
		local
			l_surface: TEXT_SURFACE_BLENDED
			l_texture: GAME_TEXTURE
		do
			create l_surface.make (a_text, a_font, a_color)
			create Result.make_from_surface (a_renderer, l_surface)
		end

feature -- Accès

	has_error: BOOLEAN
			-- Une erreur s'est produite

invariant

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"
end
