﻿note
	description: "Le moteur du jeu."
	author: "Michaël Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"

class
	ENGINE

inherit
	AUDIO_LIBRARY_SHARED
	GAME_LIBRARY_SHARED

create
	make

feature {NONE}	-- Initialisation
	make
			-- Initialise "Current"
		local
			l_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			has_error := False
			create l_builder
			l_builder.set_Dimension (768, 672)
			l_builder.set_title ("Tetris Attack")
			l_builder.enable_must_renderer_synchronize_update
			window := l_builder.generate_window

			create font.make
			create sound.make
			create forest.make (window.renderer)
			create map.make (window.renderer)
			create score.make
			create game_client.make
			create text_drawable
		end

feature -- Accès
	start
			-- Démarre le moteur du jeu
		require
			no_error: not has_error
		do
			game_library.quit_signal_actions.extend (agent on_quit)
			window.key_pressed_actions.extend (agent on_key_pressed)
			game_library.iteration_actions.extend (agent on_iteration)
			starting_timestamp := game_library.time_since_create
			game_client.launch
			game_client.receive_high_score
			if window.renderer.driver.is_software_rendering_supported then
				game_library.launch_no_delay
			else
				game_library.launch
			end
			game_client.close_socket
			game_client.join
		end

	window:GAME_WINDOW_RENDERED
			-- La {GAME_WINDOW_RENDERED} de l'application

	has_error:BOOLEAN
			-- "True" si une erreur s'est produite

	forest:FOREST
			-- La {GAME_TEXTURE} de fond du jeu

	map:MAP
			-- La {MAP} du jeu

	sound:SOUND
			-- Les {SOUND} du jeu

	score:SCORE
			-- Le {SCORE} du joueur

	game_client:SERVER_CLIENT
			-- Le client lié au serveur

	font:FONT
			-- La police de caractère

	starting_timestamp:NATURAL
			-- Le "timestamp" du début de la partie

	current_timestamp:NATURAL
			-- Le "timestamp" en cours

	text_drawable:TEXT_DRAWABLE
			-- Pour créer des textures de texte

	game_over:BOOLEAN
			-- "True" si la partie est terminée

feature {NONE}	-- Implémentation

	on_iteration(a_timestamp:NATURAL)
			-- À chaque tour de boucle de la librairie de jeu
		local
			l_worker_score:WORKER_THREAD
			l_cleared_blocks:INTEGER_32
		do
			-- Affichage
			window.renderer.clear
			window.renderer.draw_texture (forest.forest_texture, 0, 0)
			draw_map
			draw_score
			draw_high_score
			draw_timer
			if game_over then
				window.renderer.draw_texture (text_drawable.create_text_texture (window.renderer, font.font, "GAME OVER", create {GAME_COLOR}.make_rgb (255,20,110)), 350, 330)
			end
			window.renderer.present

			-- Mécanique de jeu
			l_cleared_blocks := map.clear_blocks
			score.add_score (l_cleared_blocks * 10)
			map.fall_block
			current_timestamp := a_timestamp - starting_timestamp
			check_game_over

			-- Audio
			audio_library.update

			-- Serveur
			if game_over then
				game_client.send_score(score.score)
			end
		end

	on_key_pressed(a_timestamp: NATURAL_32; a_key_event: GAME_KEY_EVENT)
			-- Action lorsqu'une touche du clavier est pesée
		do
			if not game_over then
				if a_key_event.is_space then
					map.swap_block
					sound.play_swap_sound
				elseif a_key_event.is_up then
					map.move_cursor_up
					sound.play_movement_sound
				elseif a_key_event.is_down then
					map.move_cursor_down
					sound.play_movement_sound
				elseif a_key_event.is_left then
					map.move_cursor_left
					sound.play_movement_sound
		 		elseif a_key_event.is_right then
					map.move_cursor_right
					sound.play_movement_sound
				elseif a_key_event.is_right_ctrl then
					map.add_line
					score.add_score (1)
				end
			end
		end

	on_quit(a_timestamp:NATURAL)
			-- Lorsque l'utilisateur quitte le programme
		do
			game_library.stop
		end


	check_game_over
			-- Vérifie toute les possibilités de "game_over"
		do
			if map.game_over then
				game_over := True
			elseif current_timestamp >= 120000 then
				game_over := True
			end
		end

	integer_to_string (a_value: INTEGER_32; a_leading_number: INTEGER_32): STRING
			-- Converti "a_value" en {STRING} en lui ajoutant "a_leading_number" zéros devant
		require
			leading_number_valide: a_leading_number > 0
		local
			l_index: INTEGER_32
		do
			Result := a_value.out
			from
				l_index := Result.count
			until
				l_index >= a_leading_number
			loop
				Result := "0" + Result
				l_index := l_index + 1
			end
		end


feature -- Affichage		

	draw_map
			-- Dessine la {MAP} des {BLOCK} et des curseurs
		do
			-- Dessine la {MAP} des {BLOCK}
			across
				map.grid as la_ligne
			loop
				across
					la_ligne.item as la_square
				loop
					window.renderer.draw_sub_texture_with_scale (la_square.item.texture, 0, 0, 48, 48, ((la_square.item.x - 1) * 48) + 264, ((la_square.item.y - 1) * 48) + 69, 48, 48)
				end
			end

			-- Dessine la grille des curseurs
			across
				map.cursor_grid as la_ligne
			loop
				across
					la_ligne.item as la_square
				loop
					window.renderer.draw_sub_texture_with_scale (la_square.item.texture, 0, 0, 48, 48, ((la_square.item.x - 1) * 48) + 264, ((la_square.item.y - 1) * 48) + 69, 48, 48)
				end
			end
		end

	draw_score
			-- Affiche le score à l'écran
		local
			l_score:STRING
		do
			--Affichage du mot "SCORE"
			window.renderer.draw_texture (text_drawable.create_text_texture (window.renderer, font.font, "SCORE", create {GAME_COLOR}.make_rgb (255,20,110)), 620, 95)

			--Affichage du score en cours
			l_score := integer_to_string(score.score, 5)
			window.renderer.draw_texture (text_drawable.create_text_texture (window.renderer, font.font, l_score, create {GAME_COLOR}.make_rgb (255,20,147)), 620, 130)
		end


	draw_high_score
		-- Affiche le "high_score" à l'écran
		local
			l_high_score:STRING
		do
			--Affichage du mot "HIGH_SCORE"
			window.renderer.draw_texture (text_drawable.create_text_texture (window.renderer, font.font, "HIGH SCORE", create {GAME_COLOR}.make_rgb (255,20,110)), 590, 195)

			--Affichage du score en cours
			l_high_score := integer_to_string(game_client.high_score, 5)
			window.renderer.draw_texture (text_drawable.create_text_texture (window.renderer, font.font, l_high_score, create {GAME_COLOR}.make_rgb (255,20,147)), 620, 230)
		end

	draw_timer
			-- Affiche le timer à l'écran
		local
			l_timer: STRING
		do
			--Affichage du mot "TIME"
			window.renderer.draw_texture (text_drawable.create_text_texture (window.renderer, font.font, "TIME", create {GAME_COLOR}.make_rgb (255,20,110)), 120, 65)

			--Affichage du temps restant
			if game_over then
				window.renderer.draw_texture (text_drawable.create_text_texture (window.renderer, font.font, "0", create {GAME_COLOR}.make_rgb (255,20,147)), 110, 100)
			else
				l_timer := integer_to_string(120000 - current_timestamp.as_integer_32, 3)
				window.renderer.draw_texture (text_drawable.create_text_texture (window.renderer, font.font, l_timer, create {GAME_COLOR}.make_rgb (255,20,147)), 110, 100)
			end
		end

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end
