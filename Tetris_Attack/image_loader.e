﻿note
	description: "Une classe permettant de charger des images à partir d'un fichier."
	author: "Michaël Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"

deferred class
	IMAGE_LOADER

feature {NONE} -- Initialisation

	create_texture (a_renderer: GAME_RENDERER; a_file_name: STRING): GAME_TEXTURE
			-- Initialise "Current" en utilisant "a_renderer" et "a_file_name"
		require
			valid_renderer: not a_renderer.has_error
			valid_file_name: not a_file_name.is_empty
		local
			l_image: IMG_IMAGE_FILE
		do
			create l_image.make (a_file_name)
			if l_image.is_openable then
				l_image.open
				if l_image.is_open then
					create Result.make_from_image (a_renderer, l_image)
				else
					create Result.make (a_renderer, create {GAME_PIXEL_FORMAT}, 1, 1)
					has_error := True
				end
			else
				create Result.make (a_renderer, create {GAME_PIXEL_FORMAT}, 1, 1)
			end
		ensure
			valid_renderer: not a_renderer.has_error
			valid_file_name: not a_file_name.is_empty
		end

feature

	has_error:BOOLEAN
		-- "True" si une erreur s'est produite lors de la création de "Current"

invariant

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end
