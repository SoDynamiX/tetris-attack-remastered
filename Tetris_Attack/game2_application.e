﻿note
	description : "Classe racine du programme"
	generator   : "Eiffel Game2 Project Wizard"
	date        : "7 juin 2020"
	revision    : "1.0.0"

class
	GAME2_APPLICATION

inherit
	ANY
	GAME_LIBRARY_SHARED
	IMG_LIBRARY_SHARED
	TEXT_LIBRARY_SHARED
	AUDIO_LIBRARY_SHARED
	MPG_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make
			-- Initialise les librairies et démarre le jeu
		local
			l_root_application:detachable APPLICATION
		do
			create l_root_application.make
			l_root_application := Void
			game_library.clear_all_events
			text_library.quit_library
			image_file_library.quit_library
			audio_library.quit_library
			mpg_library.quit_library
			game_library.quit_library
		end

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end
