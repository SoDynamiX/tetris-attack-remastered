﻿note
	description: "Le client du serveur."
	author: "Michaël Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"

class
	SERVER_CLIENT

inherit
	THREAD
	rename
		make as make_thread,
		execute as receive_high_score
	end

create
	make


feature {NONE} -- Initialisation

	make
			-- Créer et connecte le "socket"
		local
			l_socket: NETWORK_STREAM_SOCKET
			l_factory_address:INET_ADDRESS_FACTORY
			l_address:STRING
			l_port:INTEGER
		do
			create l_factory_address
			make_thread
			l_address := "localhost"
			l_port := 12345
			io.put_string ("Ouverture du client. Adresse: "+l_address+", port: "+l_port.out+".%N")
			if attached l_factory_address.create_from_name (l_address) as la_address then
				create l_socket.make_client_by_address_and_port (la_address, l_port)
				socket := l_socket
				if l_socket.invalid_address then
					io.put_string ("Ne peut pas se connecter a l'adresse " + l_address + ":" + l_port.out+".%N")
					has_error := True
				else
					l_socket.connect
					if not l_socket.is_connected then
						io.put_string ("Ne peut pas se connecter a l'adresse " + l_address + ":" + l_port.out+".%N")
						has_error := True
					end
				end
			end
		end

feature -- Accès

	socket: detachable NETWORK_STREAM_SOCKET
			-- Permet l'échange d'information avec le serveur

	has_error:BOOLEAN
			-- "True" si une erreur c'est produite lors de la création du "socket"

	high_score:INTEGER
			-- Le meilleur score enregistré pendant une partie

	receive_high_score
			-- Reçoit le "high_score" par le "socket"
		do
			if attached socket as la_socket then
				la_socket.put_integer (1)
				la_socket.read_integer
				high_score := la_socket.last_integer
			end
		end

	send_score(a_score:INTEGER_32)
		-- Envoie "a_score" par le "socket"
		require
			valid_socket: attached socket as la_socket and then la_socket.is_connected
		do
			if attached socket as la_socket then
				la_socket.put_integer_32 (2)
				la_socket.put_integer_32 (a_score)
				io.put_string ("Le score a ete envoye avec succes.")
			else
				io.put_string ("Le score n'a pas pu etre envoye au serveur.")
			end
		end

	close_socket
			-- Ferme le socket
		do
			if attached socket as la_socket then
				la_socket.put_integer (0)
				la_socket.close
			end
		end


note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end
