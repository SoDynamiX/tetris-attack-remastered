﻿note
    description : "Une réplique Eiffel du jeu SNES 'Tetris Attack'."
    author      : "Michaël Simoneau"
    generator   : "Eiffel Game2 Project Wizard"
    date        : "7 juin 2020"
    revision    : "1.0.0"

class
    APPLICATION

inherit
	GAME_LIBRARY_SHARED
	IMG_LIBRARY_SHARED
	AUDIO_LIBRARY_SHARED
	MPG_LIBRARY_SHARED
	TEXT_LIBRARY_SHARED

create
    make

feature {NONE} -- Initialisation

    make
            -- Démarre le jeu
        local
        	l_engine: ENGINE
        do
        	audio_library.enable_playback
			mpg_library.enable_mpg
            game_library.enable_video
            text_library.enable_text
            image_file_library.enable_image (True, False, False)
            create l_engine.make
            if not l_engine.has_error then
            	l_engine.start
            else
            	print("Il y a eu une erreur.%N")
            end

        end

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end

