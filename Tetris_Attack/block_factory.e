﻿note
	description: "L'usine des {BLOCK} pour en créer et les réutiliser."
	author: "Michaël Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"

class
	BLOCK_FACTORY

inherit
	IMAGE_LOADER
		rename
			has_error as has_loader_error
		end

create
	make

feature {NONE} -- Initialisation

	make(a_renderer:GAME_RENDERER)
			-- Initialise "Current" avec "a_renderer"
		require
			valid_renderer: not a_renderer.has_error
		do
			has_loader_error := False
			cursor_texture := create_texture(a_renderer, "images/cursor.png")
			blue_block_texture := create_texture(a_renderer, "images/blue_block.png")
			cyan_block_texture := create_texture(a_renderer, "images/cyan_block.png")
			purple_block_texture := create_texture(a_renderer, "images/purple_block.png")
			green_block_texture := create_texture(a_renderer, "images/green_block.png")
			red_block_texture := create_texture(a_renderer, "images/red_block.png")
			yellow_block_texture := create_texture(a_renderer, "images/yellow_block.png")
			blank_block_texture := create_texture(a_renderer, "images/blank_block.png")
		ensure
			valid_renderer: not a_renderer.has_error
		end

feature -- Accès

	new_cursor_block(a_x, a_y: INTEGER_32): BLOCK
		-- Création d'un {BLOCK} sous forme de curseur
		require
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		do
			create Result.make (cursor_texture)
			Result.set_x (a_x)
			Result.set_y (a_y)
			Result.set_color_value (7)
		ensure
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		end

	new_blue_block(a_x, a_y: INTEGER_32): BLOCK
		-- Création d'un {BLOCK} de texture bleu
		require
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		do
			create Result.make (blue_block_texture)
			Result.set_x (a_x)
			Result.set_y (a_y)
			Result.set_color_value (1)
		ensure
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		end

	new_cyan_block(a_x, a_y: INTEGER_32): BLOCK
		-- Création d'un {BLOCK} de texture cyan
		require
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		do
			create Result.make (cyan_block_texture)
			Result.set_x (a_x)
			Result.set_y (a_y)
			Result.set_color_value (2)
		ensure
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		end

	new_purple_block(a_x, a_y: INTEGER_32): BLOCK
		-- Création d'un {BLOCK} de texture mauve
		require
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		do
			create Result.make (purple_block_texture)
			Result.set_x (a_x)
			Result.set_y (a_y)
			Result.set_color_value (3)
		ensure
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		end

	new_green_block(a_x, a_y: INTEGER_32): BLOCK
		-- Création d'un {BLOCK} de texture vert
		require
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		do
			create Result.make (green_block_texture)
			Result.set_x (a_x)
			Result.set_y (a_y)
			Result.set_color_value (4)
		ensure
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		end

	new_red_block(a_x, a_y: INTEGER_32): BLOCK
		-- Création d'un {BLOCK} de texture rouge
		require
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		do
			create Result.make (red_block_texture)
			Result.set_x (a_x)
			Result.set_y (a_y)
			Result.set_color_value (5)
		ensure
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		end

	new_yellow_block(a_x, a_y: INTEGER_32): BLOCK
		-- Création d'un {BLOCK} de texture jaune
		require
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		do
			create Result.make (yellow_block_texture)
			Result.set_x (a_x)
			Result.set_y (a_y)
			Result.set_color_value (6)
		ensure
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		end

	new_blank_block(a_x, a_y: INTEGER_32): BLOCK
		-- Création d'un {BLOCK} de texture transparente
		require
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		do
			create Result.make (blank_block_texture)
			Result.set_x (a_x)
			Result.set_y (a_y)
			Result.set_is_empty (True)
			Result.set_color_value (0)
		ensure
			valid_x: a_x >= 1 AND a_x <= 6
			valid_y: a_y >= 1 AND a_y <= 12
		end


feature {NONE} -- Implémentation

	cursor_texture: GAME_TEXTURE
		-- Texture d'un curseur

	blue_block_texture: GAME_TEXTURE
		-- Texture d'un {BLOCK} bleu

	cyan_block_texture: GAME_TEXTURE
		-- Texture d'un {BLOCK} cyan

	purple_block_texture: GAME_TEXTURE
		-- Texture d'un {BLOCK} purple

	green_block_texture: GAME_TEXTURE
		-- Texture d'un {BLOCK} vert

	red_block_texture: GAME_TEXTURE
		-- Texture d'un {BLOCK} rouge

	yellow_block_texture: GAME_TEXTURE
		-- Texture d'un {BLOCK} jaune

	blank_block_texture: GAME_TEXTURE
		-- Texture d'un {BLOCK} transparent
invariant

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end
