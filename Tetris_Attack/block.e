﻿note
	description: "Les {BLOCK} du jeu."
	author: "Michaël Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"

class
	BLOCK

create
	make

feature {NONE} -- Initialisation

	make(a_texture:GAME_TEXTURE)
			-- Assignation d'une texture au "BLOCK"
		require
			texture_valide: not a_texture.has_error
		do
			texture := a_texture
		ensure
			texture_valide: not a_texture.has_error
		end

feature -- Accès

	x:INTEGER_32 assign set_x
		-- Position x du "BLOCK"

	y:INTEGER_32 assign set_y
		-- Position y du "BLOCK"

	set_x(a_x:INTEGER_32)
		-- Assignation d'une valeur "INTEGER_32" à x
		require
			valid_x: a_x >= 1 AND a_x <= 6
		do
			x := a_x
		ensure
			x = a_x
			valid_x: a_x >= 1 AND a_x <= 6
		end

	set_y(a_y:INTEGER_32)
		-- Assignation d'une valeur "INTEGER_32" à y
		require
			valid_y: a_y >= 1 AND a_y <= 12
		do
			y := a_y
		ensure
			y = a_y
			valid_y: a_y >= 1 AND a_y <= 12
		end

	is_empty:BOOLEAN assign set_is_empty
		-- True si le {BLOCK} est blank (block vide sans texture)

	set_is_empty(a_bool:BOOLEAN)
		-- Assignation d'une valeur "BOOLEAN" à "is_empty"
		do
			is_empty := a_bool
		ensure
			is_empty = a_bool
		end

	color_value:INTEGER_32 assign set_color_value
		-- La "color_value" du {BLOCK} (1 = bleu, 2 = cyan, 3 = mauve, 4 = vert, 5 = rouge, 6 = jaune)

	set_color_value(a_int:INTEGER_32)
		-- Assignation d'une valeur "a_int" pour la couleur du {BLOCK}
		do
			color_value := a_int
		ensure
			color_value = a_int
		end

	texture:GAME_TEXTURE
		-- Texture du "BLOCK"

invariant

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end
