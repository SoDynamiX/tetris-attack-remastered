﻿note
	description: "Fond d'écran du jeu sous forme de forêt."
	author: "Michaël Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"

class
	FOREST

inherit
	IMAGE_LOADER
		rename
			has_error as has_loader_error
		end

create
	make

feature {NONE} -- Initialisation

	make (a_renderer: GAME_RENDERER)
			-- Initialise "Current" avec "a_renderer"
		require
			valid_renderer: not a_renderer.has_error
		do
			has_loader_error := False
		 	forest_texture := create_texture(a_renderer,"images/forest.png")
		ensure
			valid_renderer: not a_renderer.has_error
		end

feature -- Implémentation

	forest_texture: GAME_TEXTURE
		-- La {GAME_TEXTURE} de la {FOREST}

invariant

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end
