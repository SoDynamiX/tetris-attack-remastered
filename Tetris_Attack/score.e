﻿note
	description: "Le score du joueur."
	author: "Michaël Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"

class
	SCORE

create
	make

feature {NONE} -- Initialisation

	make
			-- Initialise le "SCORE" à 0
		do
			score := 0
		end


feature -- Accès

	score:INTEGER
		-- Le score de la partie

feature -- Gestion du score

	add_score(a_point: INTEGER)
			-- Ajoute "a_point" au score actuel
		require
			valid_score: a_point >= 0
		do
			score := score + a_point
		end

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end

