note
	description: "Tests unitaires pour le {SERVER_CLIENT} du jeu."
	author: "Micha�l Simoneau"
	date: "28 mai 2020"
	revision: "0.1.0"
	testing: "type/manual"

class
	SERVER_CLIENT_TESTS

inherit
	EQA_TEST_SET


feature -- Test routines

	-- Test du "make" de {SERVER_CLIENT}
	normal_server_client_make_test
			-- Teste le cas normal du "server_client.make"
		note
			testing:  "covers/{SERVER_CLIENT}.make", "execution/isolated", "execution/serial"
		local
			l_game_client:SERVER_CLIENT
		do
			create l_game_client.make
			assert ("{SERVER_CLIENT}'s connection test failed.", l_game_client.has_error = false)
		end

	--Tests de la fonction "send_score"
	normal_send_score_test
			-- Teste un cas normal de la fonction "send_score"
		note
			testing:  "covers/{SERVER_CLIENT}.make, covers/{SERVER_CLIENT}.send_score",
							"execution/isolated", "execution/serial"
		local
			l_game_client:SERVER_CLIENT
		do
			create l_game_client.make
			l_game_client.send_score (500)
			assert ("{SERVER_CLIENT}'s score was not sent properly.", True)
		end

	wrong_send_score_test
			-- Teste un cas erron� de la fonction "send_score"
		note
			testing: "covers/{SERVER_CLIENT}.make, covers/{SERVER_CLIENT}.send_score",
							"covers{SERVER_CLIENT}.close_socket","execution/isolated", "execution/serial"
		local
			l_game_client:SERVER_CLIENT
			l_retry:BOOLEAN
		do
			if l_retry then
				create l_game_client.make
				l_game_client.close_socket
				l_game_client.send_score (500)
				assert ("{SERVER_CLIENT}'s score was not sent properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end


	--Tests de la fonction "receive_score"
	normal_receive_score_test
			-- Teste un cas normal de la fonction "receive_score"
		note
			testing:  "covers/{SERVER_CLIENT}.make, covers/{SERVER_CLIENT}.receive_high_score",
							"execution/isolated", "execution/serial"
		local
			l_game_client:SERVER_CLIENT
		do
			create l_game_client.make
			l_game_client.receive_high_score
			assert ("{SERVER_CLIENT} did not receive high score properly.", True)
		end

	wrong_receive_score_test
			-- Teste un cas normal de la fonction "receive_score"
		note
			testing:  "covers/{SERVER_CLIENT}.make, covers/{SERVER_CLIENT}.receive_high_score",
							"execution/isolated", "execution/serial"

		local
			l_game_client:SERVER_CLIENT
			l_retry:BOOLEAN
		do
			if l_retry then
				create l_game_client.make
				l_game_client.close_socket
				l_game_client.receive_high_score
				assert ("{SERVER_CLIENT} did not receive high score properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end
		
end


