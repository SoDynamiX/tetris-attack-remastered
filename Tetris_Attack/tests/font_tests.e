note
	description: "Tests unitaires pour l'utilisation de {FONT}."
	author: "Micha�l Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"
	testing: "type/manual"

class
	FONT_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			text_library.enable_text
			create l_window_builder
			l_window_builder.set_dimension (768, 672)
			window := l_window_builder.generate_window
			create font.make
		end

	on_clean
			-- Cette m�thode est lanc�e apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do
			text_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	normal_new_font
			-- Teste que le "make" du {FONT} n'a pas d'erreur
		note
			testing:  "covers/{FONT}.font", "execution/isolated", "covers/{FONT}.make", "covers/{FONT}.has_error",
			          "execution/serial"
		do
			assert ("Error loading font.", not font.has_error)
		end

feature {NONE} -- Impl�mentation

	window:GAME_WINDOW_RENDERED
		-- La fen�tre de l'application

	font:FONT
		-- La police de caract�re du jeu original

end


