note
	description: "Tests unitaires pour le mouvement des m�caniques de jeu."
	author: "Micha�l Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"
	testing: "type/manual"

class
	GAME_MECHANICS_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end


feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			image_file_library.enable_image (true, false, false)
			create l_window_builder
			l_window_builder.set_dimension (768, 672)
			window := l_window_builder.generate_window
			create block_factory.make (window.renderer)
			create map.make(window.renderer)
		end

	on_clean
			-- Cette m�thode est lanc�e apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do
			image_file_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	-- Tests de la fonction "swap_block"
	normal_swap_test
			-- Teste un cas normal de la fonction "swap_block"
		note
			testing:  "covers/{MAP}.swap_block", "execution/isolated", "execution/serial"
		do
			map.move_cursor_down
			map.swap_block
			assert ("{MAP}'s swap_block function did not work properly.", map.grid[map.left_cursor.x][map.left_cursor.y].color_value = 4)
			assert ("{MAP}'s swap_block function did not work properly.", map.grid[map.right_cursor.x][map.right_cursor.y].color_value = 3)
		end

	limit_swap_test_1
			-- Teste un cas limite de la fonction "swap_block"
		note
			testing:  "covers/{MAP}.swap_block", "execution/isolated", "execution/serial"
		do
			map.left_cursor.set_x (1)
			map.left_cursor.set_y (12)
			map.right_cursor.set_x (2)
			map.right_cursor.set_y (12)
			map.swap_block
			assert ("{MAP}'s swap_block function did not work properly.", map.grid[map.left_cursor.x][map.left_cursor.y].color_value = 4)
			assert ("{MAP}'s swap_block function did not work properly.", map.grid[map.right_cursor.x][map.right_cursor.y].color_value = 1)
		end

	limit_swap_test_2
			-- Teste un cas limite de la fonction "swap_block"
		note
			testing:  "covers/{MAP}.swap_block", "execution/isolated", "execution/serial"
		do
			map.left_cursor.set_x (5)
			map.left_cursor.set_y (1)
			map.right_cursor.set_x (6)
			map.right_cursor.set_y (1)
			map.swap_block
			assert ("{MAP}'s swap_block function did not work properly.", map.grid[map.left_cursor.x][map.left_cursor.y].color_value = 0)
			assert ("{MAP}'s swap_block function did not work properly.", map.grid[map.right_cursor.x][map.right_cursor.y].color_value = 0)
		end

	wrong_swap_test_1
			-- Teste un cas erron� de la fonction "swap_block"
		note
			testing:  "covers/{MAP}.swap_block", "execution/isolated", "execution/serial"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.left_cursor.set_x (0)
				map.left_cursor.set_y (15)
				map.right_cursor.set_x (1)
				map.right_cursor.set_y (15)
				map.swap_block
				assert ("{MAP}'s swap_block function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end

	wrong_swap_test_2
			-- Teste un cas erron� de la fonction "swap_block"
		note
			testing:  "covers/{MAP}.swap_block", "execution/isolated", "execution/serial"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.left_cursor.set_x (15)
				map.left_cursor.set_y (2)
				map.right_cursor.set_x (15)
				map.right_cursor.set_y (2)
				map.swap_block
				assert ("{MAP}'s swap_block function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end

	wrong_swap_test_3
			-- Teste un cas erron� de la fonction "swap_block"
		note
			testing:  "covers/{MAP}.swap_block", "execution/isolated", "execution/serial"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.left_cursor.set_x (-5)
				map.left_cursor.set_y (2)
				map.right_cursor.set_x (-5)
				map.right_cursor.set_y (2)
				map.swap_block
				assert ("{MAP}'s swap_block function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end

	wrong_swap_test_4
			-- Teste un cas erron� de la fonction "swap_block"
		note
			testing:  "covers/{MAP}.swap_block", "execution/isolated", "execution/serial"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.left_cursor.set_x (5)
				map.left_cursor.set_y (-2)
				map.right_cursor.set_x (5)
				map.right_cursor.set_y (-2)
				map.swap_block
				assert ("{MAP}'s swap_block function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end


	-- Tests de la fonction "fall_block"
	normal_fall_test
			-- Teste un cas normal de la fonction "fall_block"
		note
			testing:  "covers/{MAP}.fall_block", "execution/isolated", "execution/serial"
		do
			map.grid[1][7] := block_factory.new_blue_block (1, 7)
			map.fall_block
			assert ("{MAP}'s fall_block function did not work properly.", not map.grid[1][8].is_empty)
		end


	-- Tests de la fonction "ascend_block"
	normal_ascend_test
			-- Teste un cas normal de la fonction "ascend_block"
		note
			testing:  "covers/{MAP}.ascend_block", "execution/isolated", "execution/serial"
		do
			map.ascend_block
			assert ("{MAP}'s ascend_block function did not work properly.", map.grid[1][7].color_value = 1)
		end

	limit_ascend_test
			-- Teste un cas limite de la fonction "ascend_block"
		note
			testing:  "covers/{MAP}.ascend_block", "execution/isolated", "execution/serial"
		do
			map.grid[1][2] := block_factory.new_blue_block (1, 2)
			map.ascend_block
			assert ("{MAP}'s ascend_block function did not work properly.", not map.grid[1][1].is_empty)
		end

	wrong_ascend_test
			-- Teste un cas erron� de la fonction "ascend_block"
		note
			testing:  "covers/{MAP}.ascend_block", "execution/isolated", "execution/serial"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.grid[1][1] := block_factory.new_blue_block (1, 1)
				map.ascend_block
				assert ("{MAP}'s ascend_block function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end

	--Tests de la variable "limit_reached"
	normal_limit_reached_test
			-- Teste un cas normal de la fonction "limit_reached"
		note
			testing:  "covers/{MAP}.limit_reached", "execution/isolated", "execution/serial"
		do
			map.grid[1][1] := block_factory.new_blue_block (1, 1)
			assert ("{MAP}'s limit_reached function did not work properly.", map.limit_reached)
		end

	--Tests de la fonction "add_line"
	normal_add_line_test
			-- Teste un cas normal de la fonction "add_line"
		note
			testing:  "covers/{MAP}.add_line", "execution/isolated", "execution/serial"
		do
			map.add_line
			assert ("{MAP}'s add_line function did not work properly.", not map.grid[1][7].is_empty)
		end

	limit_add_line_test
			-- Teste un cas limite de la fonction "add_line"
		note
			testing:  "covers/{MAP}.add_line", "execution/isolated", "execution/serial"
		do
			map.grid[1][1] := block_factory.new_blue_block (1, 1)
			map.add_line
			assert ("{MAP}'s add_line function did not work properly.", map.game_over)
		end

	wrong_add_line_test
			-- Teste un cas erron� de la fonction "add_line"
		note
			testing:  "covers/{MAP}.add_line", "execution/isolated", "execution/serial"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.grid[-4][1] := block_factory.new_blue_block (-4, 1)
				map.add_line
				assert ("{MAP}'s add_line function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end


	--Tests de la fonction "vertical_clear"
	normal_vertical_clear_test
			-- Teste un cas normal de la fonction "vertical_clear"
		note
			testing:  "covers/{MAP}.vertical_clear", "execution/isolated", "execution/serial"
		do
			map.grid[4][10] := block_factory.new_green_block (4, 10)
			map.vertical_clear (map.grid[4][8], 3)
			assert ("{MAP}'s clear_blocks function did not work properly.", map.grid[4][10].is_empty)
		end

	limit_vertical_clear_test
			-- Teste un cas limite de la fonction "vertical_clear"
		note
			testing:  "covers/{MAP}.vertical_clear", "execution/isolated", "execution/serial"
		do
			map.grid[1][1] := block_factory.new_green_block (1, 1)
			map.grid[1][2] := block_factory.new_green_block (1, 2)
			map.grid[1][3] := block_factory.new_green_block (1, 3)
			map.vertical_clear (map.grid[1][1], 3)
			assert ("{MAP}'s clear_blocks function did not work properly.", map.grid[1][3].is_empty)
		end

	wrong_vertical_clear_test
			-- Teste un cas erron� de la fonction "vertical_clear"
		note
			testing:  "covers/{MAP}.add_line", "execution/isolated", "execution/serial"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.grid[1][-4] := block_factory.new_blue_block (1, -4)
				map.grid[1][-3] := block_factory.new_blue_block (1, -3)
				map.grid[1][-2] := block_factory.new_blue_block (1, -2)
				map.vertical_clear (map.grid[1][-4], 3)
				assert ("{MAP}'s add_line function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end


	--Tests de la fonction "horizontal_clear"
	normal_horizontal_clear_test
			-- Teste un cas normal de la fonction "horizontal_clear"
		note
			testing:  "covers/{MAP}.horizontal_clear", "execution/isolated", "execution/serial"
		do
			map.horizontal_clear (map.grid[1][8], 3)
			assert ("{MAP}'s horizontal_clear function did not work properly.", map.grid[3][8].is_empty)
		end

	limit_horizontal_clear_test
			-- Teste un cas limite de la fonction "horizontal_clear"
		note
			testing:  "covers/{MAP}.horizontal_clear", "execution/isolated", "execution/serial"
		do
			map.grid[1][1] := block_factory.new_green_block (1, 1)
			map.grid[2][1] := block_factory.new_green_block (2, 1)
			map.grid[3][1] := block_factory.new_green_block (3, 1)
			map.horizontal_clear (map.grid[1][1], 3)
			assert ("{MAP}'s horizontal_clear function did not work properly.", map.grid[3][1].is_empty)
		end

	wrong_horizontal_clear_test
			-- Teste un cas erron� de la fonction "horizontal_clear"
		note
			testing:  "covers/{MAP}.horizontal_clear", "execution/isolated", "execution/serial"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.grid[-4][1] := block_factory.new_blue_block (-4, 1)
				map.grid[-3][1] := block_factory.new_blue_block (-3, 1)
				map.grid[-2][1] := block_factory.new_blue_block (-2, 1)
				map.horizontal_clear (map.grid[-4][1], 3)
				assert ("{MAP}'s horizontal_clear function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end


	--Tests de la fonction "check_up_clear"
	normal_check_up_clear_test
			-- Teste un cas normal de la fonction "check_up_clear"
		note
			testing:  "covers/{MAP}.check_up_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
		do
			map.grid[4][10] := block_factory.new_green_block (4, 10)
			l_block_sequence := map.check_up_clear (map.grid[4][11])
			assert ("{MAP}'s check_up_clear function did not work properly.", l_block_sequence = 3)
		end

	limit_check_up_clear_test
			-- Teste un cas limite de la fonction "check_up_clear"
		note
			testing:  "covers/{MAP}.check_up_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
		do
			l_block_sequence := map.check_up_clear (map.grid[4][11])
			assert ("{MAP}'s check_up_clear function did not work properly.", l_block_sequence = 1)
		end

	wrong_check_up_clear_test
			-- Teste un cas erron� de la fonction "check_up_clear"
		note
			testing:  "covers/{MAP}.check_up_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block_sequence := map.check_up_clear (map.grid[14][11])
				assert ("{MAP}'s check_up_clear function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end


	--Tests de la fonction "check_down_clear"
	normal_check_down_clear_test
			-- Teste un cas normal de la fonction "check_down_clear"
		note
			testing:  "covers/{MAP}.check_down_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
		do
			map.grid[4][10] := block_factory.new_green_block (4, 10)
			l_block_sequence := map.check_down_clear (map.grid[4][9])
			assert ("{MAP}'s check_down_clear function did not work properly.", l_block_sequence = 3)
		end

	limit_check_down_clear_test
			-- Teste un cas limite de la fonction "check_down_clear"
		note
			testing:  "covers/{MAP}.check_down_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
		do
			l_block_sequence := map.check_down_clear (map.grid[4][11])
			assert ("{MAP}'s check_down_clear function did not work properly.", l_block_sequence = 1)
		end

	wrong_check_down_clear_test
			-- Teste un cas erron� de la fonction "check_down_clear"
		note
			testing:  "covers/{MAP}.check_down_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block_sequence := map.check_up_clear (map.grid[14][11])
				assert ("{MAP}'s check_down_clear function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end


	--Tests de la fonction "check_left_clear"
	normal_check_left_clear_test
			-- Teste un cas normal de la fonction "check_left_clear"
		note
			testing:  "covers/{MAP}.check_left_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
		do
			map.grid[3][11] := block_factory.new_green_block (3, 11)
			l_block_sequence := map.check_left_clear (map.grid[5][11])
			assert ("{MAP}'s check_left_clear function did not work properly.", l_block_sequence = 3)
		end

	limit_check_left_clear_test
			-- Teste un cas limite de la fonction "check_left_clear"
		note
			testing:  "covers/{MAP}.check_left_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
		do
			l_block_sequence := map.check_left_clear (map.grid[4][12])
			assert ("{MAP}'s check_left_clear function did not work properly.", l_block_sequence = 1)
		end

	wrong_check_left_clear_test
			-- Teste un cas erron� de la fonction "check_left_clear"
		note
			testing:  "covers/{MAP}.check_left_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block_sequence := map.check_left_clear (map.grid[14][11])
				assert ("{MAP}'s check_left_clear function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end


	--Tests de la fonction "check_right_clear"
	normal_check_right_clear_test
			-- Teste un cas normal de la fonction "check_right_clear"
		note
			testing:  "covers/{MAP}.check_right_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
		do
			map.grid[3][11] := block_factory.new_green_block (3, 11)
			l_block_sequence := map.check_right_clear (map.grid[3][11])
			assert ("{MAP}'s check_right_clear function did not work properly.", l_block_sequence = 3)
		end

	limit_check_right_clear_test
			-- Teste un cas limite de la fonction "check_right_clear"
		note
			testing:  "covers/{MAP}.check_right_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
		do
			l_block_sequence := map.check_right_clear (map.grid[4][12])
			assert ("{MAP}'s check_right_clear function did not work properly.", l_block_sequence = 1)
		end

	wrong_check_right_clear_test
			-- Teste un cas erron� de la fonction "check_right_clear"
		note
			testing:  "covers/{MAP}.check_right_clear", "execution/isolated", "execution/serial"
		local
			l_block_sequence:INTEGER_32
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block_sequence := map.check_right_clear (map.grid[14][11])
				assert ("{MAP}'s check_right_clear function did not work properly.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end


	--Tests de la fonction "clear_blocks"
	normal_clear_blocks_test
			-- Teste un cas normal de la fonction "clear_blocks"
		note
			testing:  "covers/{MAP}.clear_blocks", "execution/isolated", "execution/serial"
		local
			l_block_removed:INTEGER_32
		do
			l_block_removed := map.clear_blocks
			assert ("{MAP}'s clear_blocks function did not work properly.", l_block_removed = 3)
		end


feature {NONE} -- Impl�mentation

	window:GAME_WINDOW_RENDERED
		-- La fen�tre de l'application

	block_factory:BLOCK_FACTORY
		-- L'usine de "BLOCK"

	map:MAP
		-- La "map" du jeu

end


