note
	description: "Tests unitaires pour la cr�ation de {BLOCK} dans la {BLOCK_FACTORY}."
	author: "Micha�l Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"
	testing: "type/manual"

class
	BLOCK_FACTORY_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			image_file_library.enable_image (true, false, false)
			create l_window_builder
			l_window_builder.set_dimension (768, 672)
			window := l_window_builder.generate_window
			create block_factory.make (window.renderer)
		end

	on_clean
			-- Cette m�thode est lanc�e apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do
			image_file_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	--Tests de "new_blank_block"
	normal_new_blank_block
			-- Teste un cas normal de la fonction "new_blank_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blank_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_blank_block (3, 3)
			assert ("{BLOCK_FACTORY}'s new_blank_block: x is not valid (3).", l_block.x = 3)
			assert ("{BLOCK_FACTORY}'s new_blank_block: y is not valid (3).", l_block.y = 3)
		end

	limit_new_blank_block_1
			-- Teste un cas limite de la fonction "new_blank_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blank_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_blank_block (1, 1)
			assert ("{BLOCK_FACTORY}'s new_blank_block: x is not valid (1).", l_block.x = 1)
			assert ("{BLOCK_FACTORY}'s new_blank_block: y is not valid (1).", l_block.y = 1)
		end

	limit_new_blank_block_2
			-- Teste un cas limite de la fonction "new_blank_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blank_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_blank_block (6, 12)
			assert ("{BLOCK_FACTORY}'s new_blank_block: x is not valid (6).", l_block.x = 6)
			assert ("{BLOCK_FACTORY}'s new_blank_block: y is not valid (12).", l_block.y = 12)
		end

	wrong_new_blank_block_1
			-- Teste un cas erron� de la fonction "new_blank_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blank_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_blank_block (-5, 5)
				assert ("{BLOCK_FACTORY}'s new_blank_block: x is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_blank_block_2
			-- Teste un cas erron� de la fonction "new_blank_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blank_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_blank_block (8, 5)
				assert ("{BLOCK_FACTORY}'s new_blank_block: x is not valid (8).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_blank_block_3
			-- Teste un cas erron� de la fonction "new_blank_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blank_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_blank_block (3, -5)
				assert ("{BLOCK_FACTORY}'s new_blank_block: y is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_blank_block_4
			-- Teste un cas erron� de la fonction "new_blank_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blank_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_blank_block (3, 14)
				assert ("{BLOCK_FACTORY}'s new_blank_block: y is not valid (14).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end


	--Tests de "new_blue_block"	
	normal_new_blue_block
			-- Teste un cas normal de la fonction "new_blue_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blue_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_blue_block (3, 3)
			assert ("{BLOCK_FACTORY}'s new_blue_block: x is not valid (3).", l_block.x = 3)
			assert ("{BLOCK_FACTORY}'s new_blue_block: y is not valid (3).", l_block.y = 3)
		end

	limit_new_blue_block_1
			-- Teste un cas limite de la fonction "new_blue_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blue_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_blue_block (1, 1)
			assert ("{BLOCK_FACTORY}'s new_blue_block: x is not valid (1).", l_block.x = 1)
			assert ("{BLOCK_FACTORY}'s new_blue_block: y is not valid (1).", l_block.y = 1)
		end

	limit_new_blue_block_2
			-- Teste un cas limite de la fonction "new_blue_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blue_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_blue_block (6, 12)
			assert ("{BLOCK_FACTORY}'s new_blue_block: x is not valid (6).", l_block.x = 6)
			assert ("{BLOCK_FACTORY}'s new_blue_block: y is not valid (12).", l_block.y = 12)
		end

	wrong_new_blue_block_1
			-- Teste un cas erron� de la fonction "new_blue_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blue_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_blue_block (-5, 5)
				assert ("{BLOCK_FACTORY}'s new_blue_block: x is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_blue_block_2
			-- Teste un cas erron� de la fonction "new_blue_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blue_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_blue_block (8, 5)
				assert ("{BLOCK_FACTORY}'s new_blue_block: x is not valid (8).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_blue_block_3
			-- Teste un cas erron� de la fonction "new_blue_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blue_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_blue_block (3, -5)
				assert ("{BLOCK_FACTORY}'s new_blue_block: y is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_blue_block_4
			-- Teste un cas erron� de la fonction "new_blue_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_blue_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_blue_block (3, 14)
				assert ("{BLOCK_FACTORY}'s new_blue_block: y is not valid (14).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end


	--Tests de "new_cyan_block"	
	normal_new_cyan_block
			-- Teste un cas normal de la fonction "new_cyan_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_cyan_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_cyan_block (3, 3)
			assert ("{BLOCK_FACTORY}'s new_cyan_block: x is not valid (3).", l_block.x = 3)
			assert ("{BLOCK_FACTORY}'s new_cyan_block: y is not valid (3).", l_block.y = 3)
		end

	limit_new_cyan_block_1
			-- Teste un cas limite de la fonction "new_cyan_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_cyan_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_cyan_block (1, 1)
			assert ("{BLOCK_FACTORY}'s new_cyan_block: x is not valid (1).", l_block.x = 1)
			assert ("{BLOCK_FACTORY}'s new_cyan_block: y is not valid (1).", l_block.y = 1)
		end

	limit_new_cyan_block_2
			-- Teste un cas limite de la fonction "new_cyan_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_cyan_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_cyan_block (6, 12)
			assert ("{BLOCK_FACTORY}'s new_cyan_block: x is not valid (6).", l_block.x = 6)
			assert ("{BLOCK_FACTORY}'s new_cyan_block: y is not valid (12).", l_block.y = 12)
		end

	wrong_new_cyan_block_1
			-- Teste un cas erron� de la fonction "new_cyan_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_cyan_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cyan_block (-5, 5)
				assert ("{BLOCK_FACTORY}'s new_cyan_block: x is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_cyan_block_2
			-- Teste un cas erron� de la fonction "new_cyan_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_cyan_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cyan_block (8, 5)
				assert ("{BLOCK_FACTORY}'s new_cyan_block: x is not valid (8).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_cyan_block_3
			-- Teste un cas erron� de la fonction "new_cyan_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_cyan_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cyan_block (3, -5)
				assert ("{BLOCK_FACTORY}'s new_cyan_block: y is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_cyan_block_4
			-- Teste un cas erron� de la fonction "new_cyan_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_cyan_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cyan_block (3, 14)
				assert ("{BLOCK_FACTORY}'s new_cyan_block: y is not valid (14).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end


	--Tests de "new_purple_block"	
	normal_new_purple_block
			-- Teste un cas normal de la fonction "new_purple_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_purple_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_purple_block (3, 3)
			assert ("{BLOCK_FACTORY}'s new_purple_block: x is not valid (3).", l_block.x = 3)
			assert ("{BLOCK_FACTORY}'s new_purple_block: y is not valid (3).", l_block.y = 3)
		end

	limit_new_purple_block_1
			-- Teste un cas limite de la fonction "new_purple_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_purple_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_purple_block (1, 1)
			assert ("{BLOCK_FACTORY}'s new_purple_block: x is not valid (1).", l_block.x = 1)
			assert ("{BLOCK_FACTORY}'s new_purple_block: y is not valid (1).", l_block.y = 1)
		end

	limit_new_purple_block_2
			-- Teste un cas limite de la fonction "new_purple_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_purple_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_purple_block (6, 12)
			assert ("{BLOCK_FACTORY}'s new_purple_block: x is not valid (6).", l_block.x = 6)
			assert ("{BLOCK_FACTORY}'s new_purple_block: y is not valid (12).", l_block.y = 12)
		end

	wrong_new_purple_block_1
			-- Teste un cas erron� de la fonction "new_purple_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_purple_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_purple_block (-5, 5)
				assert ("{BLOCK_FACTORY}'s new_purple_block: x is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_purple_block_2
			-- Teste un cas erron� de la fonction "new_purple_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_purple_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_purple_block (8, 5)
				assert ("{BLOCK_FACTORY}'s new_purple_block: x is not valid (8).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_purple_block_3
			-- Teste un cas erron� de la fonction "new_purple_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_purple_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_purple_block (3, -5)
				assert ("{BLOCK_FACTORY}'s new_purple_block: y is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_purple_block_4
			-- Teste un cas erron� de la fonction "new_purple_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_purple_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_purple_block (3, 14)
				assert ("{BLOCK_FACTORY}'s new_purple_block: y is not valid (14).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end


	--Tests de "new_green_block"	
	normal_new_green_block
			-- Teste un cas normal de la fonction "new_green_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_green_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_green_block (3, 3)
			assert ("{BLOCK_FACTORY}'s new_green_block: x is not valid (3).", l_block.x = 3)
			assert ("{BLOCK_FACTORY}'s new_green_block: y is not valid (3).", l_block.y = 3)
		end

	limit_new_green_block_1
			-- Teste un cas limite de la fonction "new_green_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_green_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_green_block (1, 1)
			assert ("{BLOCK_FACTORY}'s new_green_block: x is not valid (1).", l_block.x = 1)
			assert ("{BLOCK_FACTORY}'s new_green_block: y is not valid (1).", l_block.y = 1)
		end

	limit_new_green_block_2
			-- Teste un cas limite de la fonction "new_green_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_green_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_green_block (6, 12)
			assert ("{BLOCK_FACTORY}'s new_green_block: x is not valid (6).", l_block.x = 6)
			assert ("{BLOCK_FACTORY}'s new_green_block: y is not valid (12).", l_block.y = 12)
		end

	wrong_new_green_block_1
			-- Teste un cas erron� de la fonction "new_green_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_green_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_green_block (-5, 5)
				assert ("{BLOCK_FACTORY}'s new_green_block: x is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_green_block_2
			-- Teste un cas erron� de la fonction "new_green_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_green_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_green_block (8, 5)
				assert ("{BLOCK_FACTORY}'s new_green_block: x is not valid (8).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_green_block_3
			-- Teste un cas erron� de la fonction "new_green_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_green_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_green_block (3, -5)
				assert ("{BLOCK_FACTORY}'s new_green_block: y is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_green_block_4
			-- Teste un cas erron� de la fonction "new_green_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_green_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_green_block (3, 14)
				assert ("{BLOCK_FACTORY}'s new_green_block: y is not valid (14).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end


	--Tests de "new_red_block"	
	normal_new_red_block
			-- Teste un cas normal de la fonction "new_red_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_red_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_red_block (3, 3)
			assert ("{BLOCK_FACTORY}'s new_red_block: x is not valid (3).", l_block.x = 3)
			assert ("{BLOCK_FACTORY}'s new_red_block: y is not valid (3).", l_block.y = 3)
		end

	limit_new_red_block_1
			-- Teste un cas limite de la fonction "new_red_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_red_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_red_block (1, 1)
			assert ("{BLOCK_FACTORY}'s new_red_block: x is not valid (1).", l_block.x = 1)
			assert ("{BLOCK_FACTORY}'s new_red_block: y is not valid (1).", l_block.y = 1)
		end

	limit_new_red_block_2
			-- Teste un cas limite de la fonction "new_red_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_red_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_red_block (6, 12)
			assert ("{BLOCK_FACTORY}'s new_red_block: x is not valid (6).", l_block.x = 6)
			assert ("{BLOCK_FACTORY}'s new_red_block: y is not valid (12).", l_block.y = 12)
		end

	wrong_new_red_block_1
			-- Teste un cas erron� de la fonction "new_red_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_red_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_red_block (-5, 5)
				assert ("{BLOCK_FACTORY}'s new_red_block: x is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_red_block_2
			-- Teste un cas erron� de la fonction "new_red_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_red_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_red_block (8, 5)
				assert ("{BLOCK_FACTORY}'s new_red_block: x is not valid (8).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_red_block_3
			-- Teste un cas erron� de la fonction "new_red_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_red_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_red_block (3, -5)
				assert ("{BLOCK_FACTORY}'s new_red_block: y is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_red_block_4
			-- Teste un cas erron� de la fonction "new_red_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_red_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_red_block (3, 14)
				assert ("{BLOCK_FACTORY}'s new_red_block: y is not valid (14).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end


	--Tests de "new_yellow_block"	
	normal_new_yellow_block
			-- Teste un cas normal de la fonction "new_yellow_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_yellow_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_yellow_block (3, 3)
			assert ("{BLOCK_FACTORY}'s new_yellow_block: x is not valid (3).", l_block.x = 3)
			assert ("{BLOCK_FACTORY}'s new_yellow_block: y is not valid (3).", l_block.y = 3)
		end

	limit_new_yellow_block_1
			-- Teste un cas limite de la fonction "new_yellow_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_yellow_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_yellow_block (1, 1)
			assert ("{BLOCK_FACTORY}'s new_yellow_block: x is not valid (1).", l_block.x = 1)
			assert ("{BLOCK_FACTORY}'s new_yellow_block: y is not valid (1).", l_block.y = 1)
		end

	limit_new_yellow_block_2
			-- Teste un cas limite de la fonction "new_yellow_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_yellow_block",
			          "execution/serial"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_yellow_block (6, 12)
			assert ("{BLOCK_FACTORY}'s new_yellow_block: x is not valid (6).", l_block.x = 6)
			assert ("{BLOCK_FACTORY}'s new_yellow_block: y is not valid (12).", l_block.y = 12)
		end

	wrong_new_yellow_block_1
			-- Teste un cas erron� de la fonction "new_yellow_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_yellow_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_yellow_block (-5, 5)
				assert ("{BLOCK_FACTORY}'s new_yellow_block: x is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_yellow_block_2
			-- Teste un cas erron� de la fonction "new_yellow_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_yellow_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_yellow_block (8, 5)
				assert ("{BLOCK_FACTORY}'s new_yellow_block: x is not valid (8).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_yellow_block_3
			-- Teste un cas erron� de la fonction "new_yellow_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_yellow_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_yellow_block (3, -5)
				assert ("{BLOCK_FACTORY}'s new_yellow_block: y is not valid (-5).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_new_yellow_block_4
			-- Teste un cas erron� de la fonction "new_yellow_block"
		note
			testing:  "execution/isolated", "covers/{BLOCK_FACTORY}.new_yellow_block",
			          "execution/serial"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_yellow_block (3, 14)
				assert ("{BLOCK_FACTORY}'s new_yellow_block: y is not valid (14).", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end


feature {NONE} -- Impl�mentation

	window:GAME_WINDOW_RENDERED
		-- La fen�tre de l'application

	block_factory:BLOCK_FACTORY
		-- L'usine de "BLOCK"

end


