note
	description: "Tests unitaires pour le {SCORE} du jeu."
	author: "Micha�l Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"
	testing: "type/manual"

class
	SCORE_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			create l_window_builder
			l_window_builder.set_dimension (768, 672)
			window := l_window_builder.generate_window
			create score.make
		end

	on_clean
			-- Cette m�thode est lanc�e apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do
			game_library.quit_library
		end

feature -- Test routines

	normal_add_score_test
			-- Teste un cas normal de la fonction "add_score"
		note
			testing:  "covers/{SCORE}.add_score", "execution/isolated", "execution/serial"
		do
			score.add_score(500)
			assert ("{SCORE}'s add_score function did not work properly.", score.score = 500)
		end

	limit_add_score_test
			-- Teste un cas limite de la fonction "add_score"
		note
			testing:  "covers/{SCORE}.add_score", "execution/isolated", "execution/serial"
		do
			score.add_score(0)
			assert ("{SCORE}'s add_score function did not work properly.", score.score = 0)
		end

	wrong_add_score_test
			-- Teste un cas erron� de la fonction "add_score"
		note
			testing:  "covers/{SCORE}.add_score", "execution/isolated", "execution/serial"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				score.add_score(-500)
			assert ("Negative value used for add_score function", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end

feature {NONE} -- Impl�mentation

	window:GAME_WINDOW_RENDERED
		-- La fen�tre de l'application

	score:SCORE
		-- Le {SCORE} du joueur

end


