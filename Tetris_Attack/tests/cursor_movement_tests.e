note
	description: "Tests unitaires pour le mouvement des {BLOCK} curseur."
	author: "Micha�l Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"
	testing: "type/manual"

class
	CURSOR_MOVEMENT_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	IMG_LIBRARY_SHARED
		undefine
			default_create
		end


feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			image_file_library.enable_image (true, false, false)
			create l_window_builder
			l_window_builder.set_dimension (768, 672)
			window := l_window_builder.generate_window
			create block_factory.make (window.renderer)
			create map.make(window.renderer)
		end

	on_clean
			-- Cette m�thode est lanc�e apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do
			image_file_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	-- Tests de nouveau curseur
	normal_new_cursor_test
			-- Teste la cr�ation normal d'un block "curseur"
		note
			testing:  "covers/{BLOCK}.x", "execution/serial", "execution/isolated", "covers/{BLOCK}.y"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_cursor_block (4, 7)
			assert ("Block.x is not valid (4).", l_block.x = 4)
			assert ("Block.y is not valid (7).", l_block.y = 7)
		end

	limit_new_cursor_test
			-- Teste la cr�ation limite d'un block "curseur"
		note
			testing:  "covers/{BLOCK}.x", "execution/serial", "execution/isolated", "covers/{BLOCK}.y"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_cursor_block (1, 1)
			assert ("Block.x is not valid (1).", l_block.x = 1)
			assert ("Block.y is not valid (1).", l_block.y = 1)
			l_block := block_factory.new_cursor_block (6, 12)
			assert ("Block.x is not valid (6).", l_block.x = 6)
			assert ("Block.y is not valid (12).", l_block.y = 12)
		end

	wrong_new_cursor_test
			-- Teste la cr�ation erron� d'un block "curseur"
		note
			testing:  "covers/{BLOCK}.x", "execution/serial", "execution/isolated", "covers/{BLOCK}.y"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cursor_block (-5, 4)
				assert ("Block.x is negative.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end

	wrong_new_cursor_test_2
			-- Teste la cr�ation erron� d'un block "curseur"
		note
			testing:  "covers/{BLOCK}.x", "execution/serial", "execution/isolated", "covers/{BLOCK}.y"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cursor_block (5, -4)
				assert ("Block.y is negative.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end

	wrong_new_cursor_test_3
			-- Teste la cr�ation erron� d'un block "curseur"
		note
			testing:  "covers/{BLOCK}.x", "execution/serial", "execution/isolated", "covers/{BLOCK}.y"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cursor_block (12, 4)
				assert ("Block.x is out of range.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end

	wrong_new_cursor_test_4
			-- Teste la cr�ation erron� d'un block "curseur"
		note
			testing:  "covers/{BLOCK}.x", "execution/serial", "execution/isolated", "covers/{BLOCK}.y"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cursor_block (5, 14)
				assert ("Block.y is out of range.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end

		end


	--Tests de "set_x"
	normal_set_x_cursor_test
			-- Teste un cas normal de la fonction "set_x"
		note
			testing:  "covers/{BLOCK}.x", "execution/serial", "execution/isolated", "covers/{BLOCK}.set_x"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_cursor_block (4, 4)
			l_block.set_x (5)
			assert ("Block.x is not valid (5).", l_block.x = 5)
		end

	limit_set_x_cursor_test
			-- Teste un cas limite de la fonction "set_x"
		note
			testing:  "covers/{BLOCK}.x", "execution/serial", "execution/isolated", "covers/{BLOCK}.set_x"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_cursor_block (4, 4)
			l_block.set_x (1)
			assert ("Block.x is not valid (1).", l_block.x = 1)
			l_block.set_x (6)
			assert ("Block.x is not valid (6).", l_block.x = 6)
		end

	wrong_set_x_cursor_test
			-- Teste un cas erron� de la fonction "set_x"
		note
			testing:  "covers/{BLOCK}.x", "execution/serial", "execution/isolated", "covers/{BLOCK}.set_x"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cursor_block (4, 4)
				l_block.set_x (20)
				assert ("Block.x is out of range.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_set_x_cursor_test_2
			-- Teste un cas erron� de la fonction "set_x"
		note
			testing:  "covers/{BLOCK}.x", "execution/serial", "execution/isolated", "covers/{BLOCK}.set_x"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cursor_block (4, 4)
				l_block.set_x (-3)
				assert ("Block.x is negative.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end


	--Tests de "set_y"
	normal_set_y_cursor_test
			-- Teste un cas normal de la fonction "set_y"
		note
			testing:  "covers/{BLOCK}.y", "execution/serial", "execution/isolated", "covers/{BLOCK}.set_y"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_cursor_block (4, 4)
			l_block.set_y (5)
			assert ("Block.y is not valid (5).", l_block.y = 5)
		end

	limit_set_y_cursor_test
			-- Teste un cas limite de la fonction "set_y"
		note
			testing:  "covers/{BLOCK}.y", "execution/serial", "execution/isolated", "covers/{BLOCK}.set_y"
		local
			l_block:BLOCK
		do
			l_block := block_factory.new_cursor_block (4, 4)
			l_block.set_y (1)
			assert ("Block.y is not valid (1).", l_block.y = 1)
			l_block.set_y (12)
			assert ("Block.y is not valid (12).", l_block.y = 12)
		end

	wrong_set_y_cursor_test
			-- Teste un cas erron� de la fonction "set_y"
		note
			testing:  "covers/{BLOCK}.y", "execution/serial", "execution/isolated", "covers/{BLOCK}.set_y"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cursor_block (4, 4)
				l_block.set_y (20)
				assert ("Block.y is out of range.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	wrong_set_y_cursor_test_2
			-- Teste un cas erron� de la fonction "set_y"
		note
			testing:  "covers/{BLOCK}.y", "execution/serial", "execution/isolated", "covers/{BLOCK}.set_y"
		local
			l_block:BLOCK
			l_retry:BOOLEAN
		do
			if not l_retry then
				l_block := block_factory.new_cursor_block (4, 4)
				l_block.set_y (-5)
				assert ("Block.y is negative.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	--Tests de "move_cursor_up"
	normal_move_cursor_up_test
			-- Teste un cas normal de la fonction "move_cursor_up"
		note
			testing:  "covers/{BLOCK}.y", "execution/serial", "execution/isolated",
				"covers/{BLOCK}.set_y" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_down"
		do
			map.left_cursor.set_x (4)
			map.left_cursor.set_y (4)
			map.right_cursor.set_x (5)
			map.right_cursor.set_y (4)

			map.move_cursor_up
			assert ("map.left_cursor.y is not valid (3).", map.left_cursor.y = 3)
			assert ("map.right_cursor.y is not valid (3).", map.right_cursor.y = 3)

			map.move_cursor_up
			assert ("map.left_cursor.y is not valid (2).", map.left_cursor.y = 2)
			assert ("map.right_cursor.y is not valid (2).", map.right_cursor.y = 2)
		end

	limit_move_cursor_up_test
			-- Teste un cas limite de la fonction "move_cursor_up"
		note
			testing:  "covers/{BLOCK}.y", "execution/serial", "execution/isolated",
				"covers/{BLOCK}.set_y" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_down"
		do
			map.left_cursor.set_x (4)
			map.left_cursor.set_y (2)
			map.right_cursor.set_x (5)
			map.right_cursor.set_y (2)

			map.move_cursor_up
			assert ("map.left_cursor.y is not valid (1).", map.left_cursor.y = 1)
			assert ("map.right_cursor.y is not valid (1).", map.right_cursor.y = 1)

			map.move_cursor_up
			assert ("map.left_cursor.y is not valid (1).", map.left_cursor.y = 1)
			assert ("map.right_cursor.y is not valid (1).", map.right_cursor.y = 1)
		end

	wrong_move_cursor_up_test
			-- Teste un cas erron� de la fonction "move_cursor_up"
		note
			testing:  "covers/{BLOCK}.y", "execution/serial", "execution/isolated", "covers/{BLOCK}.set_y"
					,"covers/{BLOCK}.x" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_up"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.left_cursor.set_x (4)
				map.left_cursor.set_y (0)
				map.right_cursor.set_x (5)
				map.right_cursor.set_y (0)

				map.move_cursor_up
				assert ("map.left_cursor and map.right_cursor out of range.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	--Tests de "move_cursor_down"
	normal_move_cursor_down_test
			-- Teste un cas normal de la fonction "move_cursor_down"
		note
			testing:  "covers/{BLOCK}.y", "execution/serial", "execution/isolated",
				"covers/{BLOCK}.set_y" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_down"
		do
			map.left_cursor.set_x (4)
			map.left_cursor.set_y (4)
			map.right_cursor.set_x (5)
			map.right_cursor.set_y (4)

			map.move_cursor_down
			assert ("map.left_cursor.y is not valid (5).", map.left_cursor.y = 5)
			assert ("map.right_cursor.y is not valid (5).", map.right_cursor.y = 5)

			map.move_cursor_down
			assert ("map.left_cursor.y is not valid (6).", map.left_cursor.y = 6)
			assert ("map.right_cursor.y is not valid (6).", map.right_cursor.y = 6)
		end

	limit_move_cursor_down_test
			-- Teste un cas limite de la fonction "move_cursor_down"
		note
			testing:  "covers/{BLOCK}.y", "execution/serial", "execution/isolated",
					"covers/{BLOCK}.set_y" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_down"
			do
			map.left_cursor.set_x (4)
			map.left_cursor.set_y (11)
			map.right_cursor.set_x (5)
			map.right_cursor.set_y (11)

			map.move_cursor_down
			assert ("map.left_cursor.y is not valid (12).", map.left_cursor.y = 12)
			assert ("map.right_cursor.y is not valid (12).", map.right_cursor.y = 12)

			map.move_cursor_down
			assert ("map.left_cursor.y is not valid (12).", map.left_cursor.y = 12)
			assert ("map.right_cursor.y is not valid (12).", map.right_cursor.y = 12)
		end

	wrong_move_cursor_down_test
			-- Teste un cas erron� de la fonction "move_cursor_down"
		note
			testing:  "covers/{BLOCK}.y", "execution/serial", "execution/isolated",
					"covers/{BLOCK}.set_y" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_down"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.left_cursor.set_x (4)
				map.left_cursor.set_y (13)
				map.right_cursor.set_x (5)
				map.right_cursor.set_y (13)

				map.move_cursor_down
				assert ("map.left_cursor and map.right_cursor out of range.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	--Tests de "move_cursor_left"
	normal_move_cursor_left_test
			-- Teste un cas normal de la fonction "move_cursor_left"
		note
			testing: "execution/serial", "execution/isolated", "covers/{BLOCK}.set_y"
					,"covers/{BLOCK}.x" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_left"
		do
			map.left_cursor.set_x (4)
			map.left_cursor.set_y (4)
			map.right_cursor.set_x (5)
			map.right_cursor.set_y (4)

			map.move_cursor_left
			assert ("map.left_cursor.x is not valid (3).", map.left_cursor.x = 3)
			assert ("map.right_cursor.x is not valid (4).", map.right_cursor.x = 4)

			map.move_cursor_left
			assert ("map.left_cursor.x is not valid (2).", map.left_cursor.x = 2)
			assert ("map.right_cursor.x is not valid (3).", map.right_cursor.x = 3)
		end

	limit_move_cursor_left_test
			-- Teste un cas limite de la fonction "move_cursor_left"
		note
			testing: "execution/serial", "execution/isolated", "covers/{BLOCK}.set_y"
					,"covers/{BLOCK}.x" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_left"
		do
			map.left_cursor.set_x (1)
			map.left_cursor.set_y (4)
			map.right_cursor.set_x (2)
			map.right_cursor.set_y (4)

			map.move_cursor_left
			assert ("map.left_cursor.x is not valid (1).", map.left_cursor.x = 1)
			assert ("map.right_cursor.x is not valid (2).", map.right_cursor.x = 2)

			map.move_cursor_left
			assert ("map.left_cursor.x is not valid (1).", map.left_cursor.x = 1)
			assert ("map.right_cursor.x is not valid (2).", map.right_cursor.x = 2)
		end

	wrong_move_cursor_left_test
			-- Teste un cas erron� de la fonction "move_cursor_left"
		note
			testing: "execution/serial", "execution/isolated", "covers/{BLOCK}.set_y"
					,"covers/{BLOCK}.x" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_left"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.left_cursor.set_x (0)
				map.left_cursor.set_y (4)
				map.right_cursor.set_x (1)
				map.right_cursor.set_y (4)

				map.move_cursor_left
				assert ("map.left_cursor out of range.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end

	--Tests de "move_cursor_right"
	normal_move_cursor_right_test
			-- Teste un cas normal de la fonction "move_cursor_right"
		note
			testing: "execution/serial", "execution/isolated", "covers/{BLOCK}.set_y"
					,"covers/{BLOCK}.x" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_right"
		do
			map.left_cursor.set_x (3)
			map.left_cursor.set_y (4)
			map.right_cursor.set_x (4)
			map.right_cursor.set_y (4)

			map.move_cursor_right
			assert ("map.left_cursor.x is not valid (4).", map.left_cursor.x = 4)
			assert ("map.right_cursor.x is not valid (5).", map.right_cursor.x = 5)

			map.move_cursor_right
			assert ("map.left_cursor.x is not valid (5).", map.left_cursor.x = 5)
			assert ("map.right_cursor.x is not valid (6).", map.right_cursor.x = 6)
		end

	limit_move_cursor_right_test
			-- Teste un cas limite de la fonction "move_cursor_right"
		note
			testing: "execution/serial", "execution/isolated", "covers/{BLOCK}.set_y"
					,"covers/{BLOCK}.x" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_right"
		do
			map.left_cursor.set_x (5)
			map.left_cursor.set_y (4)
			map.right_cursor.set_x (6)
			map.right_cursor.set_y (4)

			map.move_cursor_right
			assert ("map.left_cursor.x is not valid (5).", map.left_cursor.x = 5)
			assert ("map.right_cursor.x is not valid (6).", map.right_cursor.x = 6)

			map.move_cursor_right
			assert ("map.left_cursor.x is not valid (5).", map.left_cursor.x = 5)
			assert ("map.right_cursor.x is not valid (6).", map.right_cursor.x = 6)
		end

	wrong_move_cursor_right_test
			-- Teste un cas erron� de la fonction "move_cursor_right"
		note
			testing: "execution/serial", "execution/isolated", "covers/{BLOCK}.set_y"
					,"covers/{BLOCK}.x" ,"covers/{BLOCK}.set_x", "covers/{MAP}.move_cursor_right"
		local
			l_retry:BOOLEAN
		do
			if not l_retry then
				map.left_cursor.set_x (6)
				map.left_cursor.set_y (4)
				map.right_cursor.set_x (7)
				map.right_cursor.set_y (4)

				map.move_cursor_right
				assert ("map.right_cursor out of range.", False)
			end
		rescue
			if
				attached (create {EXCEPTIONS}).exception_manager.last_exception as la_exception and then
				la_exception.code = {EXCEP_CONST}.precondition and then
				la_exception.type_name ~ Current.generating_type.name
			then
				l_retry:= True
				retry
			end
		end


feature {NONE} -- Impl�mentation

	window:GAME_WINDOW_RENDERED
		-- La fen�tre de l'application

	block_factory:BLOCK_FACTORY
		-- L'usine de "BLOCK"

	map:MAP
		-- La "map" du jeu

end


