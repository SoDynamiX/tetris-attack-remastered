note
	description: "Tests unitaires pour la c�ration de textures de texte."
	author: "Micha�l Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"
	testing: "type/manual"

class
	TEXT_DRAWABLE_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
				l_window_builder:GAME_WINDOW_RENDERED_BUILDER
			do
				game_library.enable_video
				text_library.enable_text
				create l_window_builder
				l_window_builder.set_dimension (768, 672)
				window := l_window_builder.generate_window
				create font.make
			end

	on_clean
			-- Cette m�thode est lanc�e apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do
			text_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	normal_text_drawable_make_test
			-- Teste un cas normal de la fonction "create_text_texture"
		note
			testing:  "covers/{TEXT_DRAWABLE}.make", "execution/isolated", "execution/serial"
		local
			l_text_drawable:TEXT_DRAWABLE
		do
			create l_text_drawable
			assert ("{TEXT_DRAWABLE}'s create_text_tetxure didn't work properly.", not l_text_drawable.has_error)
		end


	--Tests de la fonction "create_text_texture"
	normal_create_text_texture_test
			-- Teste un cas normal de la fonction "create_text_texture"
		note
			testing:  "covers/{TEXT_DRAWABLE}.create_text_texture", "execution/isolated", "execution/serial"
		local
			l_texture:GAME_TEXTURE
			l_text_drawable:TEXT_DRAWABLE
		do
			create l_text_drawable
			l_texture := l_text_drawable.create_text_texture (window.renderer, font.font, "allo", create {GAME_COLOR}.make_rgb (255,20,110))
			assert ("{TEXT_DRAWABLE}'s create_text_tetxure didn't work properly.", not l_texture.has_error)
		end


feature {NONE} -- Impl�mentation

	window:GAME_WINDOW_RENDERED
		-- La fen�tre de l'application

	font:FONT
		-- La police de caract�re du jeu

end


