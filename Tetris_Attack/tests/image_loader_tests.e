note
	description: "Tests unitaires pour le chargement d'image avec {IMAGE_LOADER}."
	author: "Micha�l Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"
	testing: "type/manual"

class
	IMAGE_LOADER_TESTS

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- Cette m�thode est lanc�e avant d'ex�cuter les tests ci-dessous
			-- Permet de g�n�rer des ressources n�cessaires � l'ex�cution des
			-- tests (sans avoir besoin de le faire dans chaque m�thode de test)
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			create l_window_builder
			l_window_builder.set_dimension (768, 672)
			window := l_window_builder.generate_window
		end

	on_clean
			-- Cette m�thode est lanc�e apr�s l'ex�cution de tous les tests ci-dessous.
			-- Permet de lib�rer les ressources n�cessaires aux tests ci-dessous.
		do
			game_library.quit_library
		end

feature -- Test routines

	normal_new_forest
			-- New test routine
		note
			testing:  "execution/isolated", "covers/{IMAGE_LOADER}.has_error",
			          "covers/{IMAGE_LOADER}.create_texture", "execution/serial",
			          "covers/{FOREST}.make"
		local
			l_forest:FOREST
		do
			create l_forest.make(window.renderer)
			assert ("An error occured using the image loader.", not l_forest.has_loader_error)
			assert ("Forest texture error.", not l_forest.forest_texture.has_error)
		end

	normal_new_factory
			-- New test routine
		note
			testing:  "execution/isolated", "covers/{IMAGE_LOADER}.has_error",
			          "covers/{IMAGE_LOADER}.create_texture", "execution/serial",
			          "covers/{BLOCK_FACTORY}.make"
		local
			l_factory:BLOCK_FACTORY
		do
			create l_factory.make(window.renderer)
			assert ("An error occured using the image loader.", not l_factory.has_loader_error)
		end



feature {NONE} -- Impl�mentation

	window:GAME_WINDOW_RENDERED
		-- La fen�tre de l'application

end


