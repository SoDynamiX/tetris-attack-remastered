﻿note
	description: "Les sons du jeu."
	author: "Michaël Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"

class
	SOUND

inherit
	AUDIO_LIBRARY_SHARED
	GAME_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make
			-- Création des sources audios
		do
			create movement_sound.make ("sounds/movement.ogg")
			create swap_sound.make ("sounds/swap.ogg")
			create game_music.make ("sounds/music.ogg")

			audio_library.sources_add
			movement_sound_source := audio_library.last_source_added
			audio_library.sources_add
			swap_sound_source := audio_library.last_source_added
			audio_library.sources_add
			game_music_source := audio_library.last_source_added

			-- Vérification et démarrage des sources audios
			if game_music.is_openable and movement_sound.is_openable and swap_sound.is_openable then
				movement_sound.open
				swap_sound.open
				game_music.open
				if game_music.is_open then
					game_music_source.queue_sound_infinite_loop (game_music)
					game_music_source.play
					has_error := not game_music.is_open
				else
					has_error := true
				end
			end
		end


feature -- Gestion des effets sonores

	play_movement_sound
			-- Joue le "movement_sound"
		do
			if movement_sound_source.is_playing then
				movement_sound_source.stop
				movement_sound.restart
			end
			movement_sound_source.queue_sound (movement_sound)
			movement_sound_source.play
		end


	play_swap_sound
			-- Joue le "swap_sound"
		do
			if swap_sound_source.is_playing then
				swap_sound_source.stop
				swap_sound.restart
			end
			swap_sound_source.queue_sound (swap_sound)
			swap_sound_source.play
		end

feature -- Accès

	has_error:BOOLEAN
		-- Une erreur s'est produite

	movement_sound_source:AUDIO_SOURCE
		-- Source du "mouvement_sound"
	swap_sound_source:AUDIO_SOURCE
		-- Source du "swap_sound"
	game_music_source:AUDIO_SOURCE
		-- Source du "game_music"

	movement_sound:AUDIO_SOUND_FILE
		-- Le son lors des déplacements des curseurs
	swap_sound:AUDIO_SOUND_FILE
		-- Le son lors des "swap_block"
	game_music:AUDIO_SOUND_FILE
		-- La musique du jeu

invariant

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end
