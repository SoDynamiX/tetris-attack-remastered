﻿note
	description: "La {MAP} du jeu. Elle contiendra une grille pour les curseurs, et une pour les {BLOCK}."
	author: "Michaël Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"

class
	MAP

create
	make

feature {NONE} -- Initialisation

	make(a_renderer:GAME_RENDERER)
			-- Génération de la grille de jeu
		require
			valid_renderer: not a_renderer.has_error
		do
			-- Initialisation de {BLOCK} vides (blank_block)
			create block_factory.make (a_renderer)
			blank_block := block_factory.new_blank_block (1, 1)

			create {ARRAYED_LIST[LIST[BLOCK]]}grid.make(6)
			create {ARRAYED_LIST[LIST[BLOCK]]}cursor_grid.make(6)

			create_grid(grid, 6, 12)
			create_grid(cursor_grid, 6, 12)

			initialize_cursors
			initialize_blocks
		ensure
			valid_renderer: not a_renderer.has_error
		end

feature -- Accès

	grid: LIST[LIST[BLOCK]]
		-- La "grid" de la "MAP"

	cursor_grid: LIST[LIST[BLOCK]]
		-- La "grid" pour les curseurs de la "MAP"

	left_cursor: BLOCK
		-- Le curseur gauche

	right_cursor: BLOCK
		-- Le curseur droit

	blank_block: BLOCK
		-- Un {BLOCK} vide

	random_seed: INTEGER_32
		-- Une valeur "INTEGER_32" qui servira à changer le seed des "RANDOM"

	game_over: BOOLEAN
		-- "True" si un "ascend_block" est appelé alors que la "grid" est pleine

feature	-- Déplacement curseur

	move_cursor_up
			-- Déplace "left_cursor" et "right_cursor" en haut dans "cursor_grid"
		require
			cursor_valide: left_cursor.y >= 1 AND right_cursor.y >= 1
		do
			if left_cursor.y > 1 AND right_cursor.y > 1 then
				left_cursor.set_y (left_cursor.y - 1)
				right_cursor.set_y (right_cursor.y - 1)
				cursor_grid[left_cursor.x][left_cursor.y] := left_cursor
				cursor_grid[right_cursor.x][right_cursor.y] := right_cursor
			end
		ensure
			cursor_valide: left_cursor.y >= 1 AND right_cursor.y >= 1
		end

	move_cursor_down
			-- Déplace "left_cursor" et "right_cursor" en bas dans "cursor_grid"
		require
			cursor_valide: left_cursor.y <= 12 AND right_cursor.y <= 12
		do
			if left_cursor.y < 12 AND right_cursor.y < 12 then
				left_cursor.set_y (left_cursor.y + 1)
				right_cursor.set_y (right_cursor.y + 1)
				cursor_grid[left_cursor.x][left_cursor.y] := left_cursor
				cursor_grid[right_cursor.x][right_cursor.y] := right_cursor
			end
		ensure
			cursor_valide: left_cursor.y <= 12 AND right_cursor.y <= 12
		end

	move_cursor_left
			-- Déplace "left_cursor" et "right_cursor" à gauche dans "cursor_grid"
		require
			cursor_valide: left_cursor.x >= 1 AND right_cursor.x >= 2
		do
			if left_cursor.x > 1 AND right_cursor.x > 1 then
				left_cursor.set_x (left_cursor.x - 1)
				right_cursor.set_x (right_cursor.x - 1)
				cursor_grid[left_cursor.x][left_cursor.y] := left_cursor
				cursor_grid[right_cursor.x][right_cursor.y] := right_cursor
			end
		ensure
			cursor_valide: left_cursor.x >= 1 AND right_cursor.x >= 2
		end

	move_cursor_right
			-- Déplace "left_cursor" et "right_cursor" à droite dans "cursor_grid"
		require
			cursor_valide: left_cursor.x <= 5 AND right_cursor.x <= 6
		do
			if left_cursor.x < 6 AND right_cursor.x < 6 then
				left_cursor.set_x (left_cursor.x + 1)
				right_cursor.set_x (right_cursor.x + 1)
				cursor_grid[left_cursor.x][left_cursor.y] := left_cursor
				cursor_grid[right_cursor.x][right_cursor.y] := right_cursor
			end
		ensure
			cursor_valide: left_cursor.x <= 5 AND right_cursor.x <= 6
		end


feature -- Interactions joueur/jeu	

	swap_block
			-- Interchange les {BLOCK} à la position du "left_cursor" et du "right_cursor"
		local
			l_temp_block: BLOCK
		do
			l_temp_block := grid[left_cursor.x][left_cursor.y]
			grid[left_cursor.x][left_cursor.y] := grid[right_cursor.x][left_cursor.y]
			grid[right_cursor.x][left_cursor.y] := l_temp_block
			grid[left_cursor.x][left_cursor.y].set_x(left_cursor.x)
			grid[right_cursor.x][left_cursor.y].set_x(right_cursor.x)
		end

feature -- Mécanique de jeu

	fall_block
			-- Fait chuter les {BLOCK} de la "grid" si aucun {BLOCK} ne se trouve sous eux
		local
			l_current_block: BLOCK
		do
			blank_block := block_factory.new_blank_block (1, 1)
			across
				1 |..| 6 as i
			loop
				across
					1 |..| 11 as j
				loop
					l_current_block := grid[i.item][j.item]
					blank_block.set_x (i.item)
					blank_block.set_y (j.item)
					if l_current_block.y < 12 then
						if grid[i.item][j.item + 1].is_empty then
							grid[i.item][j.item].set_y (l_current_block.y + 1)
							grid[i.item][j.item + 1] := l_current_block
							grid[i.item][j.item] := blank_block
						end
					end
				end
			end
		end

	ascend_block
			-- Fait monter tous les {BLOCK} de la "grid"
		require
			valid_height: not limit_reached
		local
			l_current_block: BLOCK
		do
			blank_block := block_factory.new_blank_block (1, 1)
			across
				1 |..| 6 as i
			loop
				across
					1 |..| 12 as j
				loop
					l_current_block := grid[i.item][j.item]
					blank_block.set_x (i.item)
					blank_block.set_y (j.item)
					if not grid[i.item][j.item].is_empty then
						grid[i.item][j.item].set_y (l_current_block.y - 1)
						grid[i.item][j.item - 1] := l_current_block
						grid[i.item][j.item] := blank_block
					end
				end
			end
		end

	limit_reached: BOOLEAN
			-- Retourne "True" si des {BLOCK} sont situés à la plus haute rangée de la "grid"
		local
			l_bool: BOOLEAN
		do
			across
				1 |..| 6 as i
			loop
				if not grid[i.item][1].is_empty then
					l_bool := True
				end
			end
			Result := l_bool
		end

  	add_line
			-- Insère une ligne de {BLOCK} pseudo-aléatoire au bas de la "grid"
		local
			l_int: INTEGER_32
			r: RANDOM
		do
			-- On monte les {BLOCK} existants
			if not limit_reached then
				ascend_block
    			across
        			1 |..| 6 as i
        		from
        			-- Création d'un seed pseudo-aléatoire
        			create r.set_seed (random_seed)
        			r.start
    			loop
    				random_seed := random_seed + 99999
 					l_int := (r.item \\ 6 + 1)
					r.forth

					-- Création d'un {BLOCK} de couleur en fonction de la valeur de "random_seed"
    				if l_int = 1 then
    					grid[i.item][12] := block_factory.new_blue_block (i.item, 12)
    				elseif l_int = 2 then
    					grid[i.item][12] := block_factory.new_cyan_block (i.item, 12)
    				elseif l_int = 3 then
    					grid[i.item][12] := block_factory.new_purple_block (i.item, 12)
    				elseif l_int = 4 then
    					grid[i.item][12] := block_factory.new_green_block (i.item, 12)
    				elseif l_int = 5 then
    					grid[i.item][12] := block_factory.new_red_block (i.item, 12)
    				elseif l_int = 6 then
    					grid[i.item][12] := block_factory.new_yellow_block (i.item, 12)
    				end
    			end
    		else
    			game_over := true
			end
		end

	clear_blocks:INTEGER_32
			-- Effacement de {BLOCK} dans "grid" de même couleur; retourne le nombre de {BLOCK} effacé
		local
			l_up_sequence: INTEGER_32
			l_down_sequence: INTEGER_32
			l_left_sequence: INTEGER_32
			l_right_sequence: INTEGER_32
			l_cleared_blocks: INTEGER_32
		do
			l_cleared_blocks := 0
			across
				1 |..| 6 as i
			loop
				across
					1 |..| 12 as j
				loop
					if not grid[i.item][j.item].is_empty then
						-- On compte le nombre de bloc de même couleur de tous les côtés
						l_up_sequence := check_up_clear(grid[i.item][j.item])
						l_down_sequence := check_down_clear(grid[i.item][j.item])
						l_left_sequence := check_left_clear(grid[i.item][j.item])
						l_right_sequence := check_right_clear(grid[i.item][j.item])

						-- Vérification de toutes les possibilités de "clear" sur l'axe des y du {BLOCK} en cours
						if l_up_sequence = 3 then
							if l_down_sequence = 3 then
								vertical_clear(grid[i.item][j.item - 2], 5)
								l_cleared_blocks := l_cleared_blocks + 5
							elseif l_down_sequence = 2 then
								vertical_clear(grid[i.item][j.item - 2], 4)
								l_cleared_blocks := l_cleared_blocks + 4
							elseif l_down_sequence = 1 then
								vertical_clear(grid[i.item][j.item - 2], 3)
								l_cleared_blocks := l_cleared_blocks + 3
							end
						elseif l_up_sequence = 2 then
							if l_down_sequence = 3 then
								vertical_clear(grid[i.item][j.item - 1], 4)
								l_cleared_blocks := l_cleared_blocks + 4
							elseif l_down_sequence = 2 then
								vertical_clear(grid[i.item][j.item - 1], 3)
								l_cleared_blocks := l_cleared_blocks + 3
							end
						elseif l_down_sequence = 3 then
							vertical_clear(grid[i.item][j.item], 3)
							l_cleared_blocks := l_cleared_blocks + 3
						end

						-- Vérification de toutes les possibilités de "clear" sur l'axe des x du {BLOCK} en cours
						if l_left_sequence = 3 then
							if l_right_sequence = 3 then
								horizontal_clear(grid[i.item - 2][j.item], 5)
								l_cleared_blocks := l_cleared_blocks + 5
							elseif l_right_sequence = 2 then
								horizontal_clear(grid[i.item - 2][j.item], 4)
								l_cleared_blocks := l_cleared_blocks + 4
							elseif l_right_sequence = 1 then
								horizontal_clear(grid[i.item - 2][j.item], 3)
								l_cleared_blocks := l_cleared_blocks + 3
							end
						elseif l_left_sequence = 2 then
							if l_right_sequence = 3 then
								horizontal_clear(grid[i.item - 1][j.item], 4)
								l_cleared_blocks := l_cleared_blocks + 4
							elseif l_right_sequence = 2 then
								horizontal_clear(grid[i.item - 1][j.item], 3)
								l_cleared_blocks := l_cleared_blocks + 3
							end
						elseif l_right_sequence = 3 then
							horizontal_clear(grid[i.item][j.item], 3)
							l_cleared_blocks := l_cleared_blocks + 3
						end
					end
				end
			end
			Result := l_cleared_blocks
		end


	vertical_clear(a_block: BLOCK; a_int: INTEGER_32)
			-- Enlève "a_int" {BLOCK} de la "grid" verticalement à partir de "a_block"
		local
			l_int: INTEGER_32
		do
			l_int := a_int - 1
			across
				0 |..| l_int as i
			loop
				grid[a_block.x][a_block.y + i.item] := block_factory.new_blank_block(a_block.x, a_block.y + i.item)
			end
		end

	horizontal_clear(a_block: BLOCK; a_int: INTEGER_32)
			-- Enlève "a_int" {BLOCK} de la "grid" horizontalement à partir de "a_block"
		local
			l_int: INTEGER_32
		do
			l_int := a_int - 1
			across
				0 |..| l_int as i
			loop
				grid[a_block.x + i.item][a_block.y] := block_factory.new_blank_block(a_block.x + i.item, a_block.y)
			end
		end


	check_up_clear(a_block: BLOCK):INTEGER_32
			-- Vérifie si une séquence "clear" est possible au haut de "a_block" dans "grid"
		local
			l_block_left: INTEGER_32
			l_block_sequence: INTEGER_32
			l_exit: BOOLEAN
		do
			l_block_sequence := 1
			l_block_left := a_block.y - 1
			if l_block_left = 0 then
				l_exit := True
			end
			if not l_exit then
				across
					1 |..| l_block_left as i
				loop
					if not l_exit then
						if grid[a_block.x][a_block.y - i.item].color_value = a_block.color_value then
							l_block_sequence := l_block_sequence + 1
						else
							l_exit := True
						end
					end
				end
			end
			Result := l_block_sequence
		end

	check_down_clear(a_block: BLOCK):INTEGER_32
			-- Vérifie si une séquence "clear" est possible au bas de "a_block" dans "grid"
		local
			l_block_left: INTEGER_32
			l_block_sequence: INTEGER_32
			l_exit: BOOLEAN
		do
			l_block_sequence := 1
			l_block_left := 12 - a_block.y
			if l_block_left = 0 then
				l_exit := True
			end
			if not l_exit then
				across
					1 |..| l_block_left as i
				loop
					if not l_exit then
						if grid[a_block.x][a_block.y + i.item].color_value = a_block.color_value then
							l_block_sequence := l_block_sequence + 1
						else
							l_exit := True
						end
					end
				end
			end
			Result := l_block_sequence
		end

	check_left_clear(a_block: BLOCK):INTEGER_32
			-- Vérifie si une séquence "clear" est possible à gauche de "a_block" dans "grid"
		local
			l_block_left: INTEGER_32
			l_block_sequence: INTEGER_32
			l_exit: BOOLEAN
		do
			l_block_sequence := 1
			l_block_left := a_block.x - 1
			if l_block_left = 0 then
				l_exit := True
			end
			if not l_exit then
				across
					1 |..| l_block_left as i
				loop
					if not l_exit then
						if grid[a_block.x - i.item][a_block.y].color_value = a_block.color_value then
							l_block_sequence := l_block_sequence + 1
						else
							l_exit := True
						end
					end
				end
			end
			Result := l_block_sequence
		end

	check_right_clear(a_block: BLOCK):INTEGER_32
			-- Vérifie si une séquence "clear" est possible à droite de "a_block" dans "a_grid"
		local
			l_block_left: INTEGER_32
			l_block_sequence: INTEGER_32
			l_exit: BOOLEAN
		do
			l_block_sequence := 1
			l_block_left := 6 - a_block.x
			if l_block_left = 0 then
				l_exit := True
			end
			if not l_exit then
				across
					1 |..| l_block_left as i
				loop
					if not l_exit then
						if grid[a_block.x + i.item][a_block.y].color_value = a_block.color_value then
							l_block_sequence := l_block_sequence + 1
						else
							l_exit := True
						end
					end
				end
			end
			Result := l_block_sequence
		end


feature {NONE} -- Insertion

	block_factory: BLOCK_FACTORY
		-- L'usine de {BLOCK}

	create_grid(a_grid: LIST[LIST[BLOCK]]; a_length, a_height: INTEGER_32)
			-- Créer une matrice (grid) de taille "a_length" par "a_height"
		require
			a_length >= 1
			a_height >= 1
		do
			across
				1 |..| a_length as i
			loop
				a_grid.extend (create {ARRAYED_LIST[BLOCK]}.make (12))
				across
					1 |..| a_height as j
				loop
					a_grid.last.extend (blank_block)
					a_grid[i.item][j.item].set_x(i.item)
					a_grid[i.item][j.item].set_y(j.item)
				end
			end
		end

	initialize_blocks
			-- Insertion de {BLOCK} dans la "grid" de la {MAP}
		do
			across
				1 |..| 5 as i
			loop
				add_line
			end

		end

	initialize_cursors
			-- Insertion des curseurs dans la "grid" de la {MAP}
		do
			left_cursor := block_factory.new_cursor_block (3, 8)
			right_cursor := block_factory.new_cursor_block (4, 8)

			cursor_grid[left_cursor.x][left_cursor.y] := left_cursor
			cursor_grid[right_cursor.x][right_cursor.y] := right_cursor
		end


invariant

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end
