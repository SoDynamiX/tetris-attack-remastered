﻿note
	description: "La police de caractère du jeu."
	author: "Michaël Simoneau"
	date: "3 juin 2020"
	revision: "1.0.0"

class
	FONT

create
	make

feature {NONE} -- Initialisation

	make
			-- Initialise la police de caractère du jeu original
		do
			create font.make ("images/tetris-attack.ttf", 32)
			if font.is_openable then
				font.open
				font.set_outline_size (1)
			else
				has_error := True
			end
		ensure
			valid_font: not has_error
		end

feature -- Accès

	font: TEXT_FONT
			-- Style de l'écriture

	has_error: BOOLEAN
			-- Une erreur s'est produite

invariant

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"
end
