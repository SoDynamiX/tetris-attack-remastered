﻿note
	description: "Le serveur utilisé pour envoyer le {SCORE} du joueur."
	author: "Michaël Simoneau"
	date: "7 juin 2020"
	revision: "1.0.0"

class
	SERVER_APPLICATION

create
	make

feature {NONE} -- Initialisation

	make
			-- Exécute le serveur
		do
			has_error := False
			open_socket
			if not has_error then
				high_score := 1000
				await_connection
			end
		end

	open_socket
			-- Créé le "socket"
		local
			l_port:INTEGER
		do
			l_port := 12345
			io.put_string ("Ouverture du serveur sur le port: " + l_port.out + ".%N")
			create socket.make_server_by_port (l_port)
			if socket.is_bound then
				if attached socket.address as la_address then
					io.put_string ("Socket ouvert sur l'adresse:" +
									la_address.host_address.host_address + ":" +
									la_address.port.out + ".%N")
				end
			else
				io.put_string ("Impossible d'ouvrir le port " + l_port.out + ".%N")
				has_error := True
			end
		end

feature -- Accès

	socket:NETWORK_STREAM_SOCKET
			-- Permet d'obtenir des connexions de client divers

	high_score:INTEGER
			-- Le meilleur score enregistré pendant une partie

	has_error:BOOLEAN
			-- Une erreur s'est produite

	close_socket
			-- Ferme le socket
		do
			if attached socket as la_socket then
				la_socket.close
			end
		end

feature {NONE} -- Implémentation

	await_connection
			-- Met le "socket" en mode d'attente de connexion.
		require
			valid_socket: socket.is_bound
		local
			l_exit:BOOLEAN
		do
			from
				l_exit := false
			until
				l_exit
			loop
				socket.listen (1)
				socket.accept
				if attached socket.accepted as la_client_socket then
					if attached la_client_socket.peer_address as la_client_address then
						io.put_string ("Connexion client: " +
										la_client_address.host_address.host_address +
										":" + la_client_address.port.out + ".%N")
						io.put_string ("En attente du score final.%N")
					end
					server_protocol(la_client_socket)
				else
					io.put_string ("Impossible de connecter le client.%N")
				end
			end
		end


	server_protocol(a_client_socket:NETWORK_STREAM_SOCKET)
		-- Reçoit un {INTEGER} et choisi la méthode en fonction de celle-ci
		local
			l_retry:BOOLEAN -- Par défaut, l_retry sera à 'false'
			l_exit:BOOLEAN
		do
			if not l_retry then -- Si la clause 'rescue' n'a pas été utilisée, reçoit le score.
				from
				l_exit := false
				until
					l_exit
				loop
					a_client_socket.read_integer
					if a_client_socket.last_integer = 0 then
						a_client_socket.close
						l_exit := True
					elseif a_client_socket.last_integer = 1 then
						send_score(a_client_socket)
					elseif a_client_socket.last_integer = 2 then
						receive_score(a_client_socket)
					end
				end
			end
		rescue
			l_retry := True
			retry
		end


	send_score(a_client_socket:NETWORK_STREAM_SOCKET)
		-- Envoie le "high_score" au client
		do
			a_client_socket.put_integer (high_score)
		end


	receive_score(a_client_socket:NETWORK_STREAM_SOCKET)
			-- Reçoit un {INTEGER} du "a_client_socket" et affiche ces informations
		local
			l_retry:BOOLEAN		-- Par défaut, l_retry sera à "False"
		do
			if not l_retry then		-- Si la clause "rescue" n'a pas été utilisée, reçoit le score
				a_client_socket.read_integer
				io.put_string ("%NFinal score:%N" + a_client_socket.last_integer.out + "%N")
				if a_client_socket.last_integer > high_score then
					high_score := a_client_socket.last_integer
					io.put_string ("New high score!")
				end
				io.put_string ("%NWaiting for new connection...%N")
				await_connection
			else	-- Si la clause "rescue" a été utilisée, affiche un message d'erreur
				io.put_string("Le message recu n'est pas un integer valide.%N")
			end
		rescue	-- Permet d'attraper une exception
			l_retry := True
			retry
		end

note
	copyright: "Copyright (c) 2020 Michaël Simoneau"
	license: "GPL 3.0 (see http://www.gnu.org/licenses/gpl-3.0.txt)"
	source: "[
		Michaël Simoneau
		Techniques de l’informatique
		Cégep de Drummondville
	]"

end

